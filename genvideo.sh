REPOS=`find . -maxdepth 1 -type d`

echo $REPOS

for r in ${REPOS}; do
	gource --output-custom-log ${r}_log.txt ${r}
	sed -i -r "s#(.+)\|#\1|/skyhub#" ${r}_log.txt
done;

gource --output-custom-log root_log.txt .



cat *_log.txt | sort -n > combined_log.txt


gource -c 4 -s 1 -1920x1080 --auto-skip-seconds .1 --multi-sampling --stop-at-end --highlight-users --hide mouse,progress --file-idle-time 0 \
  --max-files 0 --background-colour 000000 --font-size 12  --title "Sky Hub Development"  combined_log.txt -o - | ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec h264_nvenc -pix_fmt yuv420p -crf 1 -threads 8 -bf 0 gource.mp4

